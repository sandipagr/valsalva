;--*;;;;PROGRAM DESCRIPTION;;;;;;;
;  BME 264 Project Valsalva Manuever
;  Assuming 4 MHz crystal
;  Voltage Range: 0-5V
;  @author: Sandip Agrawal
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;; Header ;;;;;;;;;;;

        list  P=PIC18F452, F=INHX32, C=160, N=0, ST=OFF, MM=OFF, R=DEC, X=ON
        #include P18F452.inc
        __CONFIG  _CONFIG1H, _XT_OSC_1H  ;XT (crystal) oscillator     ;***using XT instead
								      ;   of HS oscillator
        __CONFIG  _CONFIG2L, _PWRT_ON_2L & _BOR_ON_2L & _BORV_42_2L  ;Reset
        __CONFIG  _CONFIG2H, _WDT_OFF_2H  ;Watchdog timer disabled
        __CONFIG  _CONFIG3H, _CCP2MX_ON_3H  ;CCP2 to RC1 (rather than to RB3)
        __CONFIG  _CONFIG4L, _LVP_OFF_4L  ;RB5 enabled for I/O
        
;*****************************************************************************
; Equates, I/O, vars
;*****************************************************************************
RESET_V         EQU     0x0000          ; Address of RESET Vector
ISR_V           EQU     0x0004          ; Address of Interrupt Vector

;;;;;; the following equates are needed by the LCD subroutines

LCD_DATA        EQU     PORTD           ; LCD data lines interface
LCD_DATA_TRIS   EQU     TRISD
LCD_CTRL        EQU     PORTE           ; LCD control lines interface

LCD_LINE0       EQU     0x000
LCD_LINE1       EQU     0x040

LCD_BUSY_BIT_MASK       EQU     0x80    ;mask for busy bit

;ValsalvaThreshold		EQU		0x64	; Threshold used for Valsalva

; PORTE bits

LCD_RW          EQU     2               ; LCD Read/Write control line
LCD_E           EQU     1               ; LCD Enable control line
LCD_RS          EQU     0               ; LCD Register-Select control line

; PORTD bits
DD7             EQU     7               ; LCD dataline 7 (MSB)
DD6             EQU     6               ; LCD dataline 6
DD5             EQU     5               ; LCD dataline 5
DD4             EQU     4               ; LCD dataline 4
DD3             EQU     3               ; LCD dataline 3
DD2             EQU     2               ; LCD dataline 2
DD1             EQU     1               ; LCD dataline 1
DD0             EQU     0               ; LCD dataline 0 (LSB)
;;;;;;; Variables ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

        cblock  0x000
        LCD_TEMP                ; LCD temporary var
        TABLE_INDEX             ; LCD Index to table strings
        COUNT                   ; LCD Counter var
        DELAY                   ; LCD Delay var
        X_DELAY                 ; LCD X-delay routines
		Curr_Pressure			; Current Pressure from MouthPiece
		Curr_Cursor				; Current Cursor POsition
		MinBase_Oxi				; Minimum Base Pressure from Oximeter
		MaxBase_Oxi				; Maximum Base Pressure from Oximeter
		LoopTemp				; Temp variable to loop
		ValsalvaThreshold		; threshold for mouth pressure
		ValsalvaThresholdLow
		ValsalvaThresholdHigh	
		T0COUNT					; Tracking timer
		MinOxi					; minimum possible value
		OverShootOxi			; maximum overshoot value
		BaseLineFinal			; MaxBase_Oxi - MinBase_Oxi 
		MinFinal				; MinBase_Oxi - MinOxi
		OverShootFinal			; OverShootOxi - MinOxi
		TempCurrent
		divCounter
		MaxCount				; set this to 20
		tempBinaryDisp			; displaying Binary Value
        endc

;;;;;; Vectors ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

        org     RESET_V         ;Reset vector
        goto    Mainline        ;Branch past tables
        org     ISR_V           ;Interrupt vector
Stop    bcf		PORTC,3
		movlw	255
		call	X_Delay500
		movlw	255
		call	X_Delay500
		movlw	255
		call	X_Delay500
		movlw	255
		call	X_Delay500
		movlw	255
		call	X_Delay500
		movlw	255
		call	X_Delay500
		movlw	255
		call	X_Delay500
		movlw	255
		call	X_Delay500
		call	DispEndMsg
		goto    Stop2	        ;Branch to interrupt service routine
Stop2	goto	Stop2

;=============================================================================
; message to display
;=============================================================================


OutMsg1
                addwf   PCL ,F          ;Jump to char pointed to in W reg
                dt "Mouth Pressure:", 0     
OutMsgFail
                addwf   PCL ,F          ;Jump to char pointed to in W reg
                dt "Pressure-OutOfBounds", 0 

OverShootFailure	db	"OverShoot< MinOxi"

MinAbsFailure		db	"MinBaseOxi<MinOxi"

BaseFailure			db	"MaxBase<MinBase"

NoFailure			db	"Normal Heart!"

MildFailure			db	"Mild HeartFailure"

SevereFailure		db	"Severe HeartFailure"

StartMsg			db	"Measuring Oxi.."

ValsalvaFailure		db	"Output Failed"
PotMsg				db	"AdjustPotentiometer"
RestMsg				db	"SitBack/Relax"

;*****************************************************************************
; Initialize processor registers
	; ADInit for Oximeter
	;  	MinMax

	; ADInit for Pressure
	; Disp Pressure
	; check if the mouth pressure is above threshold
	;	if yes, start the timer and measure Oxi
	;  else loop and measure mouth pressure
;*****************************************************************************
Mainline                                        ; POWER_ON Reset (Beginning of program)
                clrf    INTCON                  ; Clear int-flags, Disable interrupts
                clrf    PCLATH                  ; Keep in lower 2KByte
                call    LcdInit                 ; Initialize LCDisplay
				movlw   LCD_LINE0
                call    LcdSDDA 
				movlw	255
				call	X_Delay500
				movlw	255
				call	X_Delay500
				movlw	255
				call	X_Delay500
				movlw	255
				call	X_Delay500
				movlw	250
				movwf	MinBase_Oxi					; Initialize MinBase_Oxi
				movlw	2
				movwf	MaxBase_Oxi					; Initialize MaxBaseOxi
				movlw	250
				movwf	MinOxi						; Initialize MinOxi	
				movlw	1
				movwf	OverShootOxi				; Initialize OverShootOxi
				movlw	0x10
				movwf	ValsalvaThreshold			; Initialize ValsalvaThreshold
				movlw	0xC
				movwf	ValsalvaThresholdLow		; Initialize ValsalvaThreshold
				movlw	0x19
				movwf	ValsalvaThresholdHigh		; Initialize ValsalvaThreshold
				movlw	20
				movwf	MaxCount
				movlw	255
				call	X_Delay500				; Pause for a while to let everything settle
				;call	DispStartMsg
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
PHASE0
;; Phase0 of the process where you check if the oximeter signal is at least 3V
; initialize ADinit
				call    ADInit					; Initialize AD conversion
				movlw	255
				call	X_Delay500
				movlw	255
				call	X_Delay500
				movlw	255
				call	X_Delay500
				call	InitOxi
				call	DispPotMsg
Phase0Loop
			call	ADStart
			movlw	0x99					; CHANGED TO 4				
			cpfslt	ADRESH				; check reading is < 103
			goto	Phase0Loop			; not less		keep on checking					
			movlw	0x38					; less so check if >57
			cpfsgt	ADRESH				; check if reading > 57
			goto 	Phase0Loop			; not greater 	
			goto	StartPhaseI			;  greater holla! 
			
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Initialize the AD to measure OxiMeter
;; Loop through to find the min and max of the Oximeter
StartPhaseI
				bcf		PORTC,3
				call	InitOxi	
;;;;;; put 10 sec long rest timer 
				call	LcdClear
				call	DispRestMsg
				movlw   0x11					; timer position dec 17
                call    LcdSDDA         		;; Position Cursor
				movlw	'1'
				call	LcdPutChar
				movlw	'0'
				call 	LcdPutChar
				movlw	's'
				call 	LcdPutChar
				movlw	1
				movwf	T0COUNT
				call 	InitializeTimer
;; The timer has been started, now needs to keep on checking the timer for 5 secs
sec5loop		movlw	0xff
				call	X_Delay500
				btfsc	INTCON, TMR0IF				; Check if Timer1 has overflowed	
				goto	sec5TimerExpired			; TMR1IF=1, call TmrExpired
				goto	sec5loop

sec5TimerExpired
				bcf		INTCON, TMR0IF			; remember to clear the flag
				tstfsz	T0COUNT					; check if zero, dont decrement if 0
				goto	NonZero5secTimer
				goto	sec3Oxi					; Timer is 0..make the Decision

NonZero5secTimer
				decf	T0COUNT,F
				movlw   0x11						; timer position dec 17
                call    LcdSDDA         			;; Position Cursor
				movlw	'0'
				call	LcdPutChar
				movf	T0COUNT, W
				addlw	0x30
				call 	LcdPutChar
				goto	sec5loop
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Baseline 3 second measurement;;;;;;;;;;;;
sec3Oxi
				call	LcdClear
				call	DispStartMsg
				movlw   0x11					; timer position dec 17
                call    LcdSDDA         		;; Position Cursor
				movlw	'0'
				call	LcdPutChar
				movlw	'2'
				call 	LcdPutChar
				movlw	's'
				call 	LcdPutChar

				movlw   0x040
				movwf	Curr_Cursor
		        call    LcdSDDA         	
				movf	MinBase_Oxi, W
				call	DispBin
				movlw	'<'
				call	LcdPutChar
				movf	MaxBase_Oxi, W
				call	DispBin
			;	call    ADInit					; Initialize AD conversion
				bcf		PORTC,3
				movlw	255
				call	X_Delay500
				movlw	255
				call	X_Delay500
				movlw	255
				call	X_Delay500
				movlw	255
				call	X_Delay500
				movlw	255
				call	X_Delay500
				movlw	255
				call	X_Delay500
				movlw	2
				movwf	T0COUNT
				call 	InitializeTimer
			

OxiReadI	
				movlw	0xff
				movwf	LoopTemp
LoopMinMaxOxi	call	ADStart
				call	FindMinMaxOxi
;				movlw   0x040
;				movwf	Curr_Cursor
;		        call    LcdSDDA         	
;				movf	MinBase_Oxi, W
;				call	DispBin
;				movlw	'<'
;				call	LcdPutChar
;				movf	MaxBase_Oxi, W
;				call	DispBin
				decfsz	LoopTemp, F
				goto	LoopMinMaxOxi

;; go to the timer to see the time
				btfsc	INTCON, TMR0IF			; Check if Timer1 has overflowed	
				goto	FirstTimerExpired			; TMR1IF=1, call TmrExpired
				goto	OxiReadI
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ADInitialize to measure the Mouth Pressure
PhaseIPressure	
				call	LcdClear
				call	TableMsg
				call	InitPressure		
				
LoopMouthThre	call	ADStart
				call	DispGraph
				movf	ADRESH, W
				cpfslt	ValsalvaThreshold
				goto	LoopMouthThre						; skip if f is less than WREG: Valsalva THreshold is not less
				goto	StartPhaseII

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Display 10 Sec  
;;	Start the 10 second timer 
;;  Measure the Oximeter Pressure
;;	

StartPhaseII
				bsf		PORTC,3
;;;;;;;;;;;;;;;;;;;;;;PAUSE;;;;;;;;;;;;;;;;
;				movlw	0x15
;				movwf	LoopTemp
;loop255			movlw	0xff
;				call	X_Delay500
;				decfsz	LoopTemp,F
;				goto	loop255
				movlw   0x11					; timer position dec 17
                call    LcdSDDA         		;; Position Cursor
				movlw	'1'
				call	LcdPutChar
				movlw	'0'
				call 	LcdPutChar
				movlw	's'
				call 	LcdPutChar
				movlw	10
				movwf	T0COUNT
				call 	InitializeTimer

;; measure 255 oximeter pressure
OxiReadII		call 	InitOxi					; Initialize Oximeter
				movlw	0xff
				call	X_Delay500
				movlw	0xff
				movwf	LoopTemp
LoopIIOxi		call	ADStart
				call	FindMinShootOxi
			;	call	LcdClear
			;	movlw	0x40
			;	call	LcdSDDA
			;	movf	MinOxi,W
			;	call	DispBin			; TO REMOVE
			;	goto	LoopIIOxi		; TO REMOVE
				decfsz	LoopTemp, F
				goto	LoopIIOxi
;; go to the timer to see the time
				btfsc	INTCON, TMR0IF			; Check if Timer1 has overflowed	
				call	TimerExpired			; TMR1IF=1, call TmrExpired

	; timer not expired..check the pressure Sensor	
				call	InitPressure
				call	ADStart
				goto	CheckPressure			; 
NormalPressure	call	DispGraph				; 
				goto	OxiReadII
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; PhaseIII
;; Initialize oximeter, timer, set t0count to 3 sec find max during that 
;; when timer becomes 0 , go to decision making
StartPhaseIII
				call 	InitOxi					; Initialize Oximeter
				call	LcdClear
				call	DispStartMsg
				movlw   0x11					; timer position dec 17
                call    LcdSDDA         		;; Position Cursor
				movlw	'0'
				call	LcdPutChar
				movlw	'3'
				call 	LcdPutChar
				movlw	's'
				call 	LcdPutChar
				movlw	0x40
				call	LcdSDDA
				movf	MinOxi,W
				call	DispBin

				movlw	3
				movwf	T0COUNT
				call 	InitializeTimer				
;; measure 255 oximeter pressure
				movlw	0xff
				call	X_Delay500

OxiReadIII		movlw	0xff
				movwf	LoopTemp
LoopIIIOxi		call	ADStart
				call	FindMaxShootOxi
				decfsz	LoopTemp, F
				goto	LoopIIIOxi
;; go to the timer to see the time
				btfsc	INTCON, TMR0IF			; Check if Timer1 has overflowed	
				goto	LastTimerExpired		; TMR1IF=1, call TmrExpired	
				goto	OxiReadIII				; timer not expired
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
DecisionMaking
				bcf		PORTC, 3
				call	LcdClear
;; find baseline by subtracting, add 1 and then divide by 2
			;	movf	MinOxi,W
			;	call	DispBin

		movf	MinBase_Oxi, W 	; move to wreg
		cpfsgt	MaxBase_Oxi 	; if MaxBase> MinBase then SKip
		goto	DispBaseFailure		
		subwf	MaxBase_Oxi,W
		movwf	BaseLineFinal

		incf	BaseLineFinal,F
		rrncf	BaseLineFinal,F
		movlw	0x7f
		andwf	BaseLineFinal,F


		incf	MinBase_Oxi,F
		rrncf	MinBase_Oxi, F
		movlw	0x7f
		andwf	MinBase_Oxi, F

		incf	MaxBase_Oxi,F
		rrncf	MaxBase_Oxi, F
		movlw	0x7f
		andwf	MaxBase_Oxi, F

		movlw	0x36
		addwf	MinBase_Oxi, F
		movlw	0x36
		addwf	MaxBase_Oxi, F

 
;; Calculate Final Parameters 
;	BaseLineFinal			; MaxBase_Oxi - MinBase_Oxi 
;	MinFinal				; MinBase_Oxi - MinOxi
;	OvershootFinal			; OverShootOxi - MinOxi		
;;; check for the normal pressure first
CheckOvershoot
		movf	MinOxi, W 					; move to wreg
		cpfsgt	OverShootOxi 				;  skip if f is greater, if MaxBase> MinBase then SKip
		goto	DispOverShootFailure		
		subwf	OverShootOxi,W
		movwf	OverShootFinal

CheckNormal
		movff	OverShootFinal, TempCurrent
		movf	BaseLineFinal, W 
		clrf	divCounter
		call	FindQuotient 		; return with quotient in divCounter
		movlw	4					; CHANGED TO 4				
		cpfsgt	divCounter			; check divcounter is > 4
		goto	CheckMin			; skip if greater, not greater
		goto	DispNormalHF	

CheckMin
		movf	MinOxi, W 		; move to wreg
		cpfsgt	MinBase_Oxi 	; skip if f is greater than:  if MinBase_Oxi > MinOxi then SKip
		goto	CheckMinAgain		
		subwf	MinBase_Oxi,W
		movwf	MinFinal
		goto	CheckMild

CheckMinAgain
		movf	MinOxi, W
		cpfseq	MinBase_Oxi
		; goto	DispMinAbsFailure
		goto	CalcAbsMinOxi
		subwf	MinBase_Oxi,W
		movwf	MinFinal
		goto	CheckMild

CalcAbsMinOxi
		movf	MinBase_Oxi,W
		subwf	MinOxi, W
		movwf	MinFinal

;; see if OverShootFinal is 5 times greater than BaseLine	
CheckMild
		;call	LcdClear
		;movlw	0x30
	;	addwf	divCounter, W
		;call	LcdPutChar

		movff	MinFinal,TempCurrent
		movf	BaseLineFinal,W
		clrf	divCounter
		call	FindQuotient
		movlw	2
		cpfsgt	divCounter
		goto	CheckSevere
		goto	DispMildHF
		
CheckSevere
	;	movlw	0x30
	;	addwf	divCounter, W
	;	call	LcdPutChar
		movf	BaseLineFinal,W 
		cpfsgt	MinFinal				; see if MinFinal is greater than BaseLineFinal
		goto	DispSevereHF			;; MinFinal is not greater
		goto	DispValsalvaFailed
		
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;	
FindQuotient
		cpfslt	TempCurrent		; compare if F is less than BaseLine
		goto	NotLessLoop		;	skip if less; f is not less
		return

NotLessLoop
		subwf	TempCurrent,F 
		incf	divCounter,F
		goto	FindQuotient

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Displaying Messages
DispEndMsg
				call	LcdClear
				movf	BaseLineFinal,W
				call	DispBin
				movlw	'*'
				call	LcdPutChar
				movf	MinFinal,W
				call	DispBin

				movlw   0x40
				movwf	Curr_Cursor
		        call    LcdSDDA         	
				movf	OverShootFinal,W
				call	DispBin

				movlw	'*'
				call	LcdPutChar
				movf	MinOxi,W
				call	DispBin
				return

DispPotMsg
		movlw	high	PotMsg
		movwf	TBLPTRH
		movlw	low		PotMsg
		movwf	TBLPTRL	
		tblrd*
		movf	TABLAT,W
		call	AgainDispLoop
		return


DispStartMsg
		movlw	high	StartMsg
		movwf	TBLPTRH
		movlw	low		StartMsg
		movwf	TBLPTRL	
		tblrd*
		movf	TABLAT,W
		call	AgainDispLoop
		return

DispRestMsg
		movlw	high	RestMsg
		movwf	TBLPTRH
		movlw	low		RestMsg
		movwf	TBLPTRL	
		tblrd*
		movf	TABLAT,W
		call	AgainDispLoop
		return

DispNormalHF
		call	LcdClear
		movlw	high	NoFailure
		movwf	TBLPTRH
		movlw	low		NoFailure
		movwf	TBLPTRL	
		tblrd*
		movf	TABLAT,W
		call	AgainDispLoop
		goto	Stop

DispMildHF
		call	LcdClear
		movlw	high	MildFailure
		movwf	TBLPTRH
		movlw	low		MildFailure
		movwf	TBLPTRL	
		tblrd*
		movf	TABLAT,W
		call	AgainDispLoop
		goto	Stop

DispSevereHF
		call	LcdClear
		movlw	high	SevereFailure
		movwf	TBLPTRH
		movlw	low		SevereFailure
		movwf	TBLPTRL	
		tblrd*
		movf	TABLAT,W
		call	AgainDispLoop
		goto	Stop

DispValsalvaFailed
		call	LcdClear
		movlw	high	ValsalvaFailure
		movwf	TBLPTRH
		movlw	low		ValsalvaFailure
		movwf	TBLPTRL	
		tblrd*
		movf	TABLAT,W
		call	AgainDispLoop
		goto	Stop

DispBaseFailure
		call	LcdClear
		movlw	high	BaseFailure
		movwf	TBLPTRH
		movlw	low		BaseFailure
		movwf	TBLPTRL	
		tblrd*
		movf	TABLAT,W
		call	AgainDispLoop
		movlw   0x040
		movwf	Curr_Cursor
        call    LcdSDDA     
		movf	MaxBase_Oxi, W
		call	DispBin
		movlw	'<'
		call	LcdPutChar
		movf	MinBase_Oxi, W
		call	DispBin
		goto	Stop

DispMinAbsFailure
		call	LcdClear
		movlw	high	MinAbsFailure
		movwf	TBLPTRH
		movlw	low		MinAbsFailure
		movwf	TBLPTRL	
		tblrd*
		movf	TABLAT,W
		call	AgainDispLoop
		movlw   0x040
		movwf	Curr_Cursor
        call    LcdSDDA         	
		movf	MinBase_Oxi, W
		call	DispBin
		movlw	'<'
		call	LcdPutChar
		movf	MinOxi, W
		call	DispBin
		goto	Stop

DispOverShootFailure
		call	LcdClear
		movlw	high	OverShootFailure
		movwf	TBLPTRH
		movlw	low		OverShootFailure
		movwf	TBLPTRL	
		tblrd*
		movf	TABLAT,W
		call	AgainDispLoop

		movlw   0x040
		movwf	Curr_Cursor
        call    LcdSDDA         	

		movf	OverShootOxi, W
		call	DispBin
		movlw	'<'
		call	LcdPutChar
		movf	MinOxi, W
		call	DispBin
		goto	Stop


AgainDispLoop
		call	LcdPutChar
		tblrd+*
		movf	TABLAT,W
		bnz		AgainDispLoop
		return
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Display the Binary Number 

DispBin
		movwf	tempBinaryDisp
		btfsc	tempBinaryDisp, 7
		goto	Disp71								; skip if 0...run if 1
		movlw	'0'
		call	LcdPutChar
		goto	Disp6
Disp71
		movlw	'1'
		call	LcdPutChar

Disp6	btfsc	tempBinaryDisp, 6
		goto	Disp61								; skip if 0...run if 1
		movlw	'0'
		call	LcdPutChar
		goto	Disp5
Disp61
		movlw	'1'
		call	LcdPutChar

Disp5	btfsc	tempBinaryDisp, 5
		goto	Disp51								; skip if 0...run if 1
		movlw	'0'
		call	LcdPutChar
		goto	Disp4
Disp51
		movlw	'1'
		call	LcdPutChar

Disp4	btfsc	tempBinaryDisp, 4
		goto	Disp41								; skip if 0...run if 1
		movlw	'0'
		call	LcdPutChar
		goto	Disp3
Disp41
		movlw	'1'
		call	LcdPutChar

Disp3	btfsc	tempBinaryDisp, 3
		goto	Disp31								; skip if 0...run if 1
		movlw	'0'
		call	LcdPutChar
		goto	Disp2
Disp31
		movlw	'1'
		call	LcdPutChar

Disp2	btfsc	tempBinaryDisp, 2
		goto	Disp21								; skip if 0...run if 1
		movlw	'0'
		call	LcdPutChar
		goto	Disp1
Disp21
		movlw	'1'
		call	LcdPutChar

Disp1	btfsc	tempBinaryDisp, 1
		goto	Disp11								; skip if 0...run if 1
		movlw	'0'
		call	LcdPutChar
		goto	Disp0
Disp11
		movlw	'1'
		call	LcdPutChar

Disp0	btfsc	tempBinaryDisp, 1
		goto	Disp01								; skip if 0...run if 1
		movlw	'0'
		call	LcdPutChar
		return
Disp01
		movlw	'1'
		call	LcdPutChar
		return

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Check if the pressure is between 30 and 50, if not go to check time
; check if time is 0 
CheckPressure
			 	movf	ADRESH, W
				cpfslt	ValsalvaThresholdLow
				goto	CheckTimeLess				; skip if f is less than WREG: Valsalva THreshold is not less, pressure is less
				cpfsgt	ValsalvaThresholdHigh
				goto	CheckTime				; skip if f is greater than WREG: Valsalva THreshold is not greater, pressure is great
				goto	NormalPressure

CheckTimeLess	tstfsz	T0COUNT						; check if t0count is zero
				goto	BreakPressure				;	not zero			
				goto	StartPhaseIII

CheckTime		tstfsz	T0COUNT						; check if t0count is zero
				goto	BreakPressure				;	not zero			
				goto	NormalPressure			

BreakPressure
				call	LcdClear
				call	TableMsgFail
				goto	Stop
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

TimerExpired
				bcf		INTCON, TMR0IF			; remember to clear the flag
				tstfsz	T0COUNT					; check if zero, dont decrement if 0
				decf	T0COUNT, F				; if not zero,,,increment variable counter, acts as the 3rd byte of timer1
				decf	MaxCount
				movlw   0x11						; timer position dec 17
                call    LcdSDDA         			;; Position Cursor
				movlw	'0'
				call	LcdPutChar
				movf	T0COUNT, W
				addlw	0x30
				call 	LcdPutChar
				tstfsz	MaxCount
				return
				goto	DispValsalvaFailed

LastTimerExpired
				bcf		INTCON, TMR0IF			; remember to clear the flag
				tstfsz	T0COUNT					; check if zero, dont decrement if 0
				goto	NonZeroTimer
				goto	DecisionMaking			; Timer is 0..make the Decision

NonZeroTimer	decf	T0COUNT,F
				movlw   0x11						; timer position dec 17
                call    LcdSDDA         			;; Position Cursor
				movlw	'0'
				call	LcdPutChar
				movf	T0COUNT, W
				addlw	0x30
				call 	LcdPutChar
				goto	OxiReadIII



FirstTimerExpired
				bcf		INTCON, TMR0IF			; remember to clear the flag
				tstfsz	T0COUNT					; check if zero, dont decrement if 0
				goto	NonZeroFirstTimer
				goto	PhaseIPressure			; Timer is 0..make the Decision

NonZeroFirstTimer
				decf	T0COUNT,F
				movlw   0x11						; timer position dec 17
                call    LcdSDDA         			;; Position Cursor
				movlw	'0'
				call	LcdPutChar
				movf	T0COUNT, W
				addlw	0x30
				call 	LcdPutChar
				goto	OxiReadI


								
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
ADStart         
				bsf     ADCON0, GO              ; Start A/D Conversion
                nop
ADLoop          
                btfsc   ADCON0, NOT_DONE 		; wait for conversion to finish
                goto    ADLoop
                return							; go on to next conversion
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
InitPressure
				bcf     ADCON0, 3       		; Select AN3/RA3 Channel
                bcf     ADCON0, 4
                bsf     ADCON0, 5	
				return

InitOxi			
				bcf     ADCON0, 3       		; 2 Select AN3/RA3 Channel
                bsf     ADCON0, 4
                bcf     ADCON0, 5
				return
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Computes Min and Max of the Oximeter Pressure and Stores it in MinBase_Oxi and MaxBase_Oxi
FindMinMaxOxi	
				movlw	4
				cpfsgt	ADRESH	
				return
				movf	ADRESH, W 			; Oxi in WREG
;				tstfsz	ADRESH				; skip if 0
;				goto	Advance1
;				return
Advance1		cpfslt	MinBase_Oxi			; compare if less
				movwf	MinBase_Oxi			; if MinBase is not Less, make w as f
			   	cpfsgt	MaxBase_Oxi			; compare f and Wreg
				movwf	MaxBase_Oxi			; if not greater than
				return						; if MaxBase is already greater

FindMinShootOxi
				
				movlw	4
				cpfsgt	ADRESH	
				return
				movf	ADRESH, W 			; Oxi in WREG
				;tstfsz	ADRESH				; skip if 0
			;	goto	Advance2
			;	return
Advance2		cpfslt	MinOxi				; compare if less
				movwf	MinOxi				; if MinBase is not Less, make w as f
			   	return						; if MaxBase is already greater

FindMaxShootOxi
				movf	ADRESH, W 			; Oxi in WREG
				cpfsgt	OverShootOxi		; compare f and Wreg
				movwf	OverShootOxi			; if not greater than
				return						; if MaxBase is already greater
		
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Takes the A/D result and display it as the bar graph in the second column

DispGraph
				movff	ADRESH, Curr_Pressure
				movff	LCD_LINE1, Curr_Cursor
				movlw   0x040
				movwf	Curr_Cursor
                call    LcdSDDA         		;; Position Cursor
				tstfsz	Curr_Pressure			;; test if the Pressure is 0
				goto	DispLoop				;; if not zero	
				goto	DispBlank				; if zero

DispLoop		
				movlw	0x23
				call	LcdPutChar
				incf	Curr_Cursor,F
				movlw	2
				cpfsgt	Curr_Pressure
				goto	DispBlank
				subwf	Curr_Pressure, F
				goto	DispLoop

DispBlank		movlw	0x54
				cpfslt	Curr_Cursor
				return				; if not less	
				movlw	' '
				call	LcdPutChar
				incf	Curr_Cursor,F
				goto	DispBlank
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

InitializeTimer
	clrf	TMR0H		
	clrf	TMR0L
	movlw	b'10000011'	;1:32 prescalar
	movwf	T0CON		;enable count input to TMR1
	return
				

;***************************************************************************
;
; (2) "ADInit" subroutine:
; A/D Initialization routine.  Check out the PIC18FXX2 datasheet.  
; 2 and 4 are to be initialized
;****************************************************************************
ADInit                                  
				movlw   b'01000001'     ; Initialize ADCON1
                movwf   ADCON0	
				
;					bsf     ADCON0, 0       ; Power Up A/D Module
;                bsf     ADCON0, 6       ; Fosc/4 Conversion Clock
;                bcf     ADCON0, 7
;                bcf     ADCON0, 2       ; A/D Conversion NOT in Progress
;                bcf     ADCON0, 3       ; Select AN2/RA2 Channel
;                bcf     ADCON0, 4
;                bcf     ADCON0, 5

                movlw   b'00000010'     ; Initialize ADCON1
                movwf   ADCON1
             	bsf		TRISA, 4		; Set proper PORTA pins for Input
				bsf		TRISA, 2
				clrf	PORTC
				bsf		PORTC, 3
				movlw	0xf0
				movwf	TRISC
			;	bcf		TRISA, 6
			;	bcf		PORTA, 6
                return
                
;*****************************************************************************
; Send a message using a table to output a message
; OK
;*****************************************************************************
TableMsg
                movlw   0               ; Startindex of table message
DispMsg
                movwf   TABLE_INDEX     ; Holds message address
                call    OutMsg1
                andlw   0x0FF           ; Check if at end of message
                btfsc   STATUS, Z       ; (zero returned at end)
                goto    TableMsgEnd             
                call    LcdPutChar      ; Display character
                movf    TABLE_INDEX, W  ; Point to next character
                addlw   2
                goto    DispMsg
TableMsgEnd     return



TableMsgFail				
                movlw   0               ; Startindex of table message
DispMsgFail
                movwf   TABLE_INDEX     ; Holds message address
                call    OutMsgFail
                andlw   0x0FF           ; Check if at end of message
                btfsc   STATUS, Z       ; (zero returned at end)
                goto    TableMsgEndFail             
                call    LcdPutChar      ; Display character
                movf    TABLE_INDEX, W  ; Point to next character
                addlw   2
                goto    DispMsgFail
TableMsgEndFail return    

;*****************************************************************************
; LCD Module Subroutines
;*****************************************************************************
;
;=============================================================================
; LcdInit
; Initilize LC-Display Module
; Should be modified to your needs (i.e. display type, cursor on/off, etc.)
; OK
;=============================================================================
LcdInit
              	movlw   0x07
                movwf   ADCON1          ; set port e to digital
                clrf    PORTD           ; ALL LCD PORT outputs should be Low.
                clrf    PORTE

                clrf    TRISE
                clrf    LCD_DATA_TRIS   ; set data port tris
                                        ; Busy-flag is not yet valid
                clrf    LCD_CTRL        ; ALL PORT output should output Low.
                                        ; power-up delay
                movlw   100
                call    X_Delay500      ; 100 * 0.5mS = 50mS
                                        ; Busy Flag should be valid from here
                movlw   0x038           ; command lcd into 8-bit-interface, 2 lines

                call    LcdPutCmd
                movlw   0x000           ; disp.off, curs.off, no-blink
                call    LcdDMode
                call    LcdClear
                movlw   d'4'            ; disp.on, curs.off
                call    LcdDMode
                movlw   0x002           ; auto-inc (shift-cursor)
                call    LcdEMode
                return


;=============================================================================
; LcdBusy
; Returns when LCD busy-flag is inactive
; OK
;=============================================================================
LcdBusy
                movlw   0x0FF                   ; Set PORTD for input
                movwf   LCD_DATA_TRIS
                bcf     LCD_CTRL, LCD_RS        ; Set LCD for command mode
                bsf     LCD_CTRL, LCD_RW        ; Setup to read busy flag
                bsf     LCD_CTRL, LCD_E         ; LCD E-line High
                movf    LCD_DATA, W             ; Read busy flag + DDram address
                bcf     LCD_CTRL, LCD_E         ; LCD E-line Low
                andlw   LCD_BUSY_BIT_MASK       ; Check Busy flag, High = Busy
                btfss   STATUS, Z               ; skip if set
                goto    LcdBusy
LcdNotBusy      bcf     LCD_CTRL, LCD_RW
                clrf    LCD_DATA_TRIS           ; Set data port for output
                return

;=============================================================================
; LcdClear
; Clears display and returns cursor to home position (upper-left corner).
;
;=============================================================================
LcdClear
                movlw   0x001
                call    LcdPutCmd
                return

;=============================================================================
; LcdHome
; Returns cursor to home position.
; Returns display to original position (when shifted).
;
;=============================================================================
LcdHome
                movlw   0x002
                call    LcdPutCmd
                return
;=============================================================================
; LcdEMode
; Sets entry mode of display.
; Required entry mode must be set in W
;  b0   : 0 = no display shift  1 = display shift
;  b1   : 0 = auto-decrement    1 = auto-increment
;  b2-7 : don't care
; OK
;=============================================================================
LcdEMode
                andlw   0x003           ; Strip upper bits
                iorlw   0x004           ; Function set
                call    LcdPutCmd
                return

;=============================================================================
; LcdDMode
; Sets display control.
; Required display mode must be set in W
;  b0   : 0 = cursor blink off  1 = cursor blink on
;  b1   : 0 = cursor off        1 = cursor on
;  b2   : 0 = display off       1 = display on (display data remains in DDRAM)
;  b3-7 : don't care
; OK
;=============================================================================
LcdDMode
                andlw   0x007           ; Strip upper bits
                iorlw   0x008           ; Function set
                call    LcdPutCmd
                return

;=============================================================================
; LcdSCGA
; Sets Character-Generator-RAM address. CGRAM is read/written after
;  this setting.
; Required CGRAM address must be set in W
;  b0-5 : required CGRAM address
;  b6-7 : don't care
;
;=============================================================================
LcdSCGA
                andlw   0x03F           ; Strip upper bits
                iorlw   0x040           ; Function set
                call    LcdPutCmd
                return

;=============================================================================
; LcdSDDA
; Sets the Display-Data-RAM address. DDRAM data is read/written after
;  this setting.
; Required DDRAM address must be set in W
;  b0-6 : required DDRAM address
;  b7   : don't care
; OK
;=============================================================================
LcdSDDA
                iorlw   0x080           ; Function set
                call    LcdPutCmd
                return

;=============================================================================
; LcdGAddr
; Returns address counter contents, used for both DDRAM and CGRAM.
; RAM address is returned in W
;
;=============================================================================
LcdGAddr
                movlw   0x0FF                   ; Set Data PORT for input
                movwf   LCD_DATA_TRIS
                bcf     LCD_CTRL, LCD_RS        ; Set LCD for command mode
                bsf     LCD_CTRL, LCD_RW        ; Setup to read busy flag
                BSF     LCD_CTRL, LCD_E         ; LCD E-line High
                movf    LCD_DATA, W             ; Read busy flag + RAM address
                bcf     LCD_CTRL, LCD_E         ; LCD E-line Low
                andlw   0x07F                   ; Strip upper bit
                bcf     LCD_CTRL, LCD_RW
                movlw   0x000
                movwf   LCD_DATA_TRIS           ; Set Data PORT for output
                return

;=============================================================================
; LcdPutChar
; Sends character to LCD
; Required character must be in W
; OK
;=============================================================================
LcdPutChar
                movwf   LCD_TEMP                ; Character to be sent is in W
                call    LcdBusy                 ; Wait for LCD to be ready
                bcf     LCD_CTRL, LCD_RW        ; Set LCD in read mode
                bsf     LCD_CTRL, LCD_RS        ; Set LCD in data mode
                bsf     LCD_CTRL, LCD_E         ; LCD E-line High
                movf    LCD_TEMP, W
                movwf   LCD_DATA                ; Send data to LCD
                bcf     LCD_CTRL, LCD_E         ; LCD E-line Low
                return

;=============================================================================
; LcdPutCmd
; Sends command to LCD
; Required command must be in W
; OK
;=============================================================================
LcdPutCmd
                movwf   LCD_TEMP                ; Command to be sent is in W
                call    LcdBusy                 ; Wait for LCD to be ready
                bcf     LCD_CTRL, LCD_RW        ; Set LCD in read mode
                bcf     LCD_CTRL, LCD_RS        ; Set LCD in command mode
                bsf     LCD_CTRL, LCD_E         ; LCD E-line High

                movf    LCD_TEMP, W
                movwf   LCD_DATA                ; Send data to LCD
                bcf     LCD_CTRL, LCD_E         ; LCD E-line Low
                return



;*****************************************************************************
; Delay_time    = ((DELAY_value * 3) + 4) * Cycle_time
; DELAY_value   = (Delay_time - (4 * Cycle_time)) / (3 * Cycle_time)
;
; i.e. (@ 4MHz crystal)
; Delay_time    = ((32 * 3) + 4) * 1uSec
;               = 100uSec
; DELAY_value   = (500uSec - 4) / 3
;               = 165.33
;               = 165
;*****************************************************************************
Delay500        movlw   165             ; +1            1 cycle
                movwf   DELAY           ; +2            1 cycle
Delay500_Loop   decfsz  DELAY, F        ; step 1        1 cycle
                goto    Delay500_Loop   ; step 2        2 cycles
                return                  ; +3            2 cycles
;
;
X_Delay500      movwf   X_DELAY         ; +1            1 cycle
X_Delay500_Loop call    Delay500        ; step1         wait 500uSec
                decfsz  X_DELAY, F      ; step2         1 cycle
                goto    X_Delay500_Loop ; step3         2 cycles
                return                  ; +2            2 cycles

;************************************************************************
;
; (3) "ScaleA2D" subroutine:
; Assume that A/D conversion is completed before the PIC executes this 
; subroutine, now you have to convert this digital result to a voltage 
; value.  
; Hint: Since A/D conversion result is only 10 bit, it's sufficient to use 
; "FXM1616U", a 16bit by 16bit multiplication subroutine.
;
; Also, you may find it helpful to think of 5V as 5000mV in your scaling.
; It will make displaying the result a little bit simpler, but don't
; forget to display the voltage in volts on the LCD.
;  
;*************************************************************************
;
;ScaleA2D
;                movf    ADRESH, W       ; Move MSB of A/D Result
;                movwf   AARGB0
;                movf    ADRESL, W       ; Move LSB of A/D Result
;                movwf   AARGB1
;
;                movlw   h'00'
;                movwf   BARGB0
;                movlw   h'01'           ; Move LSB of 4883 (Scaling Factor)
;                movwf   BARGB1
;                call    FXM1616U        ; Perform Conversion
;                return


	;INCLUDE NECESSARY MATH FILES
 	;include "c:\math18\mathvars.inc"
 	;include "c:\math18\fxd2424u.inc"
	;include "c:\math18\fxm1616u.inc"         
        END                             ; End of program
